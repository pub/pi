/*
* rayonpi.rs
* Neil Gershenfeld 12/24/24
* Rust Rayon parallel pi calculation benchmark
* pi = 3.14159265358979323846
*/

use std::time::SystemTime;
use std::thread::available_parallelism;
use rayon::prelude::*;

const NPTS:u64 = 1e9 as u64;

fn main() {
   let a:f64 = 0.5;
   let b:f64 = 0.75;
   let c:f64 = 0.25;
   //
   let start = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
   let pi:f64 = (1..=NPTS).into_iter()
      .map(|i| a/(((i as f64)-b)*((i as f64)-c)))
      .sum();
   let end = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
   let dt = ((end-start) as f64)/1e6;
   let mflops = (NPTS as f64)*5.0/((dt as f64)*1e6);
   println!("NPTS = {NPTS}, cores = 1, pi = {pi}");
   println!("time = {dt:.3}, estimated MFlops = {mflops:.0}");
   //
   let num_cores:u64 = available_parallelism().unwrap().get() as u64;
   let start = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
   let npts:u64 = NPTS*num_cores;
   let pi:f64 = (1..=npts).into_par_iter()
      .map(|i| a/(((i as f64)-b)*((i as f64)-c)))
      .sum();
   let end = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
   let dt = ((end-start) as f64)/1e6;
   let mflops = (npts as f64)*5.0/((dt as f64)*1e6);
   println!("NPTS = {npts}, cores = {num_cores}, pi = {pi}");
   println!("time = {dt:.3}, estimated MFlops = {mflops:.0}");
   }
