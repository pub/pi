/*
* pi.rs
* Neil Gershenfeld 12/21/24
* Rust pi calculation benchmark
* pi = 3.14159265358979323846
*/

use std::time::SystemTime;

const NPTS:u64 = 1e9 as u64;

fn main() {
    let a:f64 = 0.5;
    let b:f64 = 0.75;
    let c:f64 = 0.25;
    let mut pi:f64 = 0.0;
    let start = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
    for i in 1..=NPTS {
        pi += a/(((i as f64)-b)*((i as f64)-c));
        }
    let end = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros();
    let dt = ((end-start) as f64)/1e6;
    let mflops = (NPTS as f64)*5.0/((dt as f64)*1e6);
    println!("NPTS = {NPTS}, pi = {pi}");
    println!("time = {dt:.3}, estimated MFlops = {mflops:.0}");
}
