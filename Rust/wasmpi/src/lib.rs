/*
* lib.rs
* Neil Gershenfeld 12/26/24
* Rust WebAssembly pi calculation benchmark
* pi = 3.14159265358979323846
*/

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn rust_serial_pi(npts:u32) -> f64{
   let a:f64 = 0.5;
   let b:f64 = 0.75;
   let c:f64 = 0.25;
   let mut pi:f64 = 0.0;
   for i in 1..=npts {
       pi += a/(((i as f64)-b)*((i as f64)-c));
       }
   return pi;
   }

#[wasm_bindgen]
pub fn rust_serial_map_pi(npts:u32) -> f64{
   let a:f64 = 0.5;
   let b:f64 = 0.75;
   let c:f64 = 0.25;
   let pi:f64 = (1..=npts).into_iter()
      .map(|i| a/(((i as f64)-b)*((i as f64)-c)))
      .sum();
   return pi;
   }
