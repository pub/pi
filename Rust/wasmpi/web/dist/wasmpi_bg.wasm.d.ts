/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export const rust_serial_pi: (a: number) => number;
export const rust_serial_map_pi: (a: number) => number;
export const __wbindgen_export_0: WebAssembly.Table;
export const __wbindgen_start: () => void;
