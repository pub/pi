//
// workerpi.js
// Neil Gershenfeld 12/11/18
// pi calculation benchmark
// pi = 3.14159265358979323846
//
const {Worker} = require('worker_threads')
var points = 1e9
var thread =
   `
   const Worker = require('worker_threads')
   Worker.parentPort.on('message',(msg) => {
      var index = msg.index
      var points = msg.points
      var a = 0.5
      var b = 0.75
      var c = 0.25
      var sum = 0
      var start = 1+points*index
      var end = points*(index+1)
      for (var i = start; i <= end; ++i)
         sum += a/((i-b)*(i-c))
      Worker.parentPort.postMessage(sum)
   })
   `
var workers = parseInt(process.argv[2])
var results = 0
var pi = 0
var tstart = Date.now()/1000
for (let i = 0; i < workers; ++i) {
   var worker = new Worker(thread,{eval:true})
   worker.on('message',(sum) => {
      results += 1
      pi += sum
      if (results == workers) {
         var tend = Date.now()/1000
         var mflops = (workers*points)*5.0*1e-6/(tend-tstart)
         console.log('pi: '+pi)
         console.log('time: '+(tend-tstart).toFixed(1)+'s')
         console.log('workers: '+workers)
         console.log('estimated MFlops: '+mflops.toFixed(1))
         process.exit()
         }
      })
   worker.postMessage({index:i,points:points})
   }
