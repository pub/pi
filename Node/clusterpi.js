//
// clusterpi.js
// Neil Gershenfeld 12/8/18
// pi calculation benchmark
// pi = 3.14159265358979323846
//
const cluster = require('cluster')
if (cluster.isMaster) 
   master()
else
   child()
function master() {
   var processes = parseInt(process.argv[2])
   var points = 1e9
   var pi = 0
   var results = 0
   var tstart = Date.now()/1000
   for (var i = 0; i < processes; i++) {
      var worker = cluster.fork({index:i,points:points})
      worker.on('message',(result) => {
         pi += result
         results += 1
         if (results == processes) {
            var tend = Date.now()/1000
            var mflops = (processes*points)*5.0*1e-6/(tend-tstart)
            console.log('pi: '+pi)
            console.log('time: '+(tend-tstart).toFixed(1)+'s')
            console.log('processes: '+processes)
            console.log('estimated MFlops: '+mflops.toFixed(1))
            process.exit()
            }
         })
      }
   }
function child() { 
   var index = parseInt(process.env.index)
   var points = parseInt(process.env.points)
   var a = 0.5
   var b = 0.75
   var c = 0.25
   var sum = 0
   var start = 1+points*index
   var end = points*(index+1)
   for (var i = start; i <= end; ++i)
      sum += a/((i-b)*(i-c))
   process.send(sum)
   }
