//
// forkspi.js
// Neil Gershenfeld 12/8/18
// pi calculation benchmark
// pi = 3.14159265358979323846
//
var index = parseInt(process.argv[2])
var points = parseInt(process.argv[3])
var a = 0.5
var b = 0.75
var c = 0.25
var sum = 0
var start = 1+points*index
var end = points*(index+1)
for (var i = start; i <= end; ++i)
   sum += a/((i-b)*(i-c))
process.send(sum)
