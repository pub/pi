//
// forkpi.js
// Neil Gershenfeld 12/8/18
// pi calculation benchmark
// pi = 3.14159265358979323846
//
const points = 1e9
const { fork } = require('child_process')
var processes = parseInt(process.argv[2])
var pi = 0
var results = 0
var tstart = Date.now()/1000
for (let i = 0; i < processes; ++i) {
   const child = fork('forkspi.js',[i,points])
   child.on('message',(result) => {
   pi += result
   results += 1
      if (results == processes) {
         var tend = Date.now()/1000
         var mflops = (processes*points)*5.0*1e-6/(tend-tstart)
         console.log('pi: '+pi)
         console.log('time: '+(tend-tstart).toFixed(1)+'s')
         console.log('processes: '+processes)
         console.log('estimated MFlops: '+mflops.toFixed(1))
         process.exit()
         }
      })
   }
