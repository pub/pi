#
# cupi.py
# Neil Gershenfeld 1/23/17
# calculation of pi by a CuPy sum
# pi = 3.14159265358979323846 
#

import numpy as np
import cupy as cp
import time

NPTS = 100000000
start_time = time.time()
i = np.arange(1,(NPTS+1),dtype=np.float64)
pi = np.sum(0.5/((i-0.75)*(i-.25)))
end_time = time.time()
mflops = NPTS*5.0/(1.0e6*(end_time-start_time))
print("NPTS = %d, pi = %f"%(NPTS,pi))
print("time = %f, estimated NumPy MFlops = %f"%(end_time-start_time,mflops))

NPTS = 100000000
start_time = time.time()
i = cp.arange(1,(NPTS+1),dtype=cp.float64)
pi = cp.sum(0.5/((i-0.75)*(i-.25)))
end_time = time.time()
mflops = NPTS*5.0/(1.0e6*(end_time-start_time))
print("NPTS = %d, pi = %f"%(NPTS,pi))
print("time = %f, estimated CuPy MFlops = %f"%(end_time-start_time,mflops))
