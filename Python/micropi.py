#
# micropi.py
# Neil Gershenfeld 12/26/22
# calculation of pi by a MicroPython sum
# pi = 3.14159265358979323846 
#

import time,machine

NPTS = 1000000

@micropython.native
def calc():
   a = 0.5
   b = 0.75
   c = 0.25
   pi = 0
   for i in range(1,(NPTS+1)):
      pi += a/((i-b)*(i-c))
   return pi

machine.freq(250000000)
start_time = time.time()
pi = calc()
end_time = time.time()
mflops = NPTS*5.0/(1.0e6*(end_time-start_time))
print("NPTS = %d, pi = %f"%(NPTS,pi))
print("time = %f, estimated MFlops = %f"%(end_time-start_time,mflops))
