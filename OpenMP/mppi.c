/*
* mppi.c
* Neil Gershenfeld 6/21/19
* OpenMP pi calculation benchmark
* pi = 3.14159265358979323846
*/

#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <stdint.h>

#define NPTS 100000000000

void main() {
   uint64_t i;
   double a,b,c,pi,dt,mflops;
   struct timespec tstart,tend;
   clock_gettime(CLOCK_REALTIME,&tstart);
   a = 0.5;
   b = 0.75;
   c = 0.25;
   pi = 0;
   pi = 0;
   #pragma omp parallel for reduction(+:pi)
   for (i = 1; i <= NPTS; ++i)
      pi += a/((i-b)*(i-c));
   clock_gettime(CLOCK_REALTIME,&tend);
   dt = (tend.tv_sec+tend.tv_nsec/1e9)-(tstart.tv_sec+tstart.tv_nsec/1e9);
   mflops = NPTS*5.0/(dt*1e6);
   printf("NPTS = %ld, pi = %f, threads = %d\n",NPTS,pi,omp_get_max_threads());
   printf("time = %f, estimated MFlops = %f\n",dt,mflops);
   }
