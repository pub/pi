# [pi calculation benchmark](pi.pdf)

|estimated GFlops|code|description|system|date|
|---|---|---|---|---|
|17,340,800|[cudampipi.cu](CUDA/cudampipi.cu)|C++, CUDA+MPI<br>2048 nodes, 12228 ranks/GPUs<br>nvcc -arch=sm_70 -std=c++11|Summit<br>Oak Ridge OLCF<br>IBM AC922|December, 2020|
|88,333|[mpimppi.c](hybrid/mpimppi.c)|C, MPI+OpenMP<br>1024 nodes, 64 cores/node, 4 threads/core<br>cc mpimppi.c -o mpimppi -O3 -ffast-math -fopenmp|Theta<br>Argonne ALCF<br>Cray XC40|October, 2019|
|16,239|[cudapit.cu](CUDA/cudapit.cu)|C++, CUDA, 8 GPUs, 6192 cores/GPU|NVIDIA A100|December, 2020|
|12,589|[cudapit.cu](CUDA/cudapit.cu)|C++, CUDA, 8 GPUs, 5120 cores/GPU|NVIDIA V100|March, 2020|
|11,083|[mpithreadpi.cpp](hybrid/mpithreadpi.cpp)|C++, MPI+threads, 128 nodes, 64 cores/node, 4 threads/core<br>CC mpithreadpi.cpp -o mpithreadpi -O3 -ffast-math -std=c++11|Theta<br>Argonne ALCF<br>Cray XC40|March, 2020|
|2,117|[mpipi2.c](MPI/mpipi2.c)|C, MPI, 10 nodes, 96 cores/node<br>mpicc mpipi2.c -o mpipi2 -O3 -ffast-math|Intel 2x Xeon Platinum 8175M|October, 2019|
|2,102|[mpipi2.py](Python/mpipi2.py)|Python, Numba, MPI<br>10 nodes, 96 cores/node|Intel 2x Xeon Platinum 8175M|February, 2020|
|2,052|[cudapi.cu](CUDA/cudapi.cu)|C++, CUDA, 6192 cores|NVIDIA A100|December, 2020|
|1,635|[cudapi.cu](CUDA/cudapi.cu)|C++, CUDA, 5120 cores|NVIDIA V100|March, 2020|
|1,595|prior|IBM Blue Gene/P|C, MPI, 4096 processes|prior|
|1,090|[numbapig.py](Python/numbapig.py)|Python, Numba, CUDA, 5120 cores|NVIDIA V100|March, 2020|
|1,062|[taichipi.py](Python/taichipi.py)|Python, Taichi, 5120 cores|NVIDIA V100|March, 2023|
|811|prior|Cray XT4|C, MPI, 2048 processes|prior|
|604|[jaxpi.py](Python/jaxpi.py)|Python, Jax, 5120 cores|NVIDIA V100|December, 2024|
|501|[rayonpi.rs](Rust/rayonpi.rs)|Rust, Rayon, 96 cores<br>cargo run --release|Graviton4|December, 2024|
|484|[threadpi.rs](Rust/threadpi.rs)|Rust, threads, 96 cores<br>cargo run --release -- 96|Graviton4|December, 2024|
|315|[numbapip.py](Python/numbapip.py)|Python, Numba, parallel, fastmath<br>96 cores|Intel 2x Xeon Platinum 8175M|February, 2020|
|272|[threadpi.c](C/threadpi.c)|C, 96 threads<br>gcc threadpi.c -o threadpi -O3 -ffast-math -pthread|Intel 2x Xeon Platinum 8175M|June, 2019|
|267|[threadpi.cpp](C++/threadpi.cpp)|C++, 96 threads<br>g++ threadpi.cpp -o threadpi -O3 -ffast-math -pthread|Intel 2x Xeon Platinum 8175M|March, 2020|
|211|[mpipi2.c](MPI/mpipi2.c)|C, MPI, 1 node, 96 cores<br>mpicc mpipi2.c -o mpipi2 -O3 -ffast-math|Intel 2x Xeon Platinum 8175M|October, 2019|
|180|[mpipi2.py](Python/mpipi2.py)|Python, Numba, MPI<br>mpirun -np 96 python mpipi2.py|Intel 2x Xeon Platinum 8175M|February, 2020|
|173|[mppi.c](OpenMP/mppi.c)|C, OpenMP, 96 threads<br>gcc mppi.c -o mppi -O3 -ffast-math -fopenmp|Intel 2x Xeon Platinum 8175M|July, 2019|
|152|[pi.html](https://pub.pages.cba.mit.edu/pi/JavaScript/pi.html)|JavaScript, 96 workers|Intel 2x Xeon Platinum 8175M|June, 2019|
|93.2|[threadpi.c](C/threadpi.c)|C, 56 threads<br>gcc threadpi.c -o threadpi -O3 -ffast-math -pthread|Intel 2x E5-2680|December, 2018|
|71.4|[pi.html](https://pub.pages.cba.mit.edu/pi/JavaScript/pi.html)|JavaScript, 56 workers|Intel 2x E5-2680|November, 2018|
|46.9|[mpipi.c](MPI/mpipi.c)|C, MPI<br>mpicc mpipi.c -o mpipi -O3 -ffast-math <br> mpirun -np 6 mpipi|Intel i7-8700T|November, 2018|
|44.6|[threadpi.c](C/threadpi.c)|C, 6 threads<br>gcc threadpi.c -o threadpi -O3 -ffast-math -pthread|Intel i7-8700T|December, 2018|
|23.3|[mpipi2.py](Python/mpipi2.py)|Python, Numba, MPI<br>mpirun -np 6 python mpipi2.py|Intel i7-8700T|February, 2020|
|16.1|[pi.html](https://pub.pages.cba.mit.edu/pi/JavaScript/pi.html)|JavaScript, 6 workers|Intel i7-8700T|November, 2018|
|15.7|[clusterpi.js](Node/clusterpi.js)|Node, 6 workers|Intel i7-8700T|December, 2018|
|9.37|[pi.c](C/pi.c)|C<br>gcc pi.c -o pi -lm -O3 -ffast-math|Intel i7-8700T|November, 2018|
|5.55|[rayonpi.rs](Rust/rayonpi.rs)|Rust, Rayon, 1 core<br>cargo run --release|Graviton4|December, 2024|
|4.87|[numbapi.py](Python/numbapi.py)|Python, Numba|Intel i7-8700T|February, 2020|
|4.63|[pi.c](C/pi.c)|C<br>gcc pi.c -o pi -lm -O3|Intel i7-8700T|December, 2024|
|3.73|[pi.html](https://pub.pages.cba.mit.edu/pi/JavaScript/pi.html)|JavaScript, 1 worker|Intel i7-8700T|November, 2018|
|3.47|[pi.html](https://pub.pages.cba.mit.edu/pi/JavaScript/pi.html)|JavaScript, 1 worker|Intel 2x E5-2680|November, 2018|
|3.29|[pi.js](Node/pi.js)|Node|Intel i7-8700T|December, 2018|
|3.19|[pi.rs](Rust/pi.rs)|Rust<br>cargo run --release|Intel i7-8700T|December, 2024|
|3.12|[clusterpi.js](Node/clusterpi.js)|Node, 1 worker|Intel i7-8700T|December, 2018|
|2.66|[wasmpi.html](https://gitlab.cba.mit.edu/pub/pi/-/tree/master/Rust/wasmpi)|Rust, WebAssembly<br>wasm-pack build --target web --out-dir web/dist|Intel i7-8700T|December, 2024|
|1.78|[threadpi.c](C/threadpi.c)|C, 4 threads<br>gcc threadpi.c -o threadpi -O3 -ffast-math -pthread|Raspberry Pi 4|December, 2020|
|1.21|[cupi.py](Python/cupi.py)|Python, CuPy, 5120 cores|NVIDIA V100|March, 2023|
|0.85|prior|Connection Machine CM-2|C, 32k processors|prior|
|0.57|[pi.c](C/pi.c)|C<br>gcc pi.c -o pi -lm|Intel i7-8700T|November, 2018|
|0.47|[numpi.py](Python/numpi.py)|Python, NumPy|Intel i7-8700T|November, 2018|
|0.26|[multipi.cpp](https://gitlab.cba.mit.edu/zfredin/dice_firmware/-/blob/753157eb3624b1053ec95d1d3a042d4ba9b2c09a/src/pi/multipi.cpp)|C/C++|[Static DICE](http://cba.mit.edu/docs/papers/20.09.DICE.pdf) (16 SAMD51J20A DICE nodes)|August, 2020|
|0.148|prior|IBM ES/9000|C|prior|
|0.134|prior|Pentium III|C|prior|
|0.118|prior|Cray Y-MP4/464|C, vector|prior|
|0.074|[pi.c](C/pi.c)|C<br>gcc pi.c -o pi -lm -O3 -ffast-math|Raspberry Pi Zero|December, 2020|
|0.029|[pi.py](Python/pi.py)|Python|Intel i7-8700T|November, 2018|
|0.0168|[pi.c](https://gitlab.cba.mit.edu/zfredin/samd51_benchmarks)|C<br>floats, -O3, gcc-arm-none-eabi, 160 MHz|SAMD51J20A<br>ARM Cortex M4F|October, 2019|
|0.013|prior|Intel Pentium Pro|C|prior|
|0.0128|[pi.c](https://gitlab.cba.mit.edu/zfredin/stm32f412_core/tree/master/nucleo-f412zg/pi)|C<br>floats, -O3, gcc-arm-none-eabi, 84 MHz|STM32F412<br>ARM Cortex M4F|October, 2019|
|0.010|prior|Cray Y-MP4/464|C, scalar|prior|
|0.006|[pi.ino](Arduino/pi.ino)|Arduino, floats|ESP32-WROOM|December, 2020|
|0.0026|[pi.ino](Arduino/pi.ino)|Arduino, floats, 250 MHz, -O3|RP2040|December, 2022|
|0.001|prior|Sun SPARCStation 1|C|prior|
|0.001|prior|DEC VAX 8650|C|prior|
|0.0007|prior|Intel 486|C|prior|
|0.00031|[micropi.py](Python/micropi.py)|MicroPython, 250 MHz|RP2040|December, 2022|
|0.0002|[pi.ino](Arduino/pi.ino)|Arduino, floats|ATSAMD21E|December, 2020|
|0.0001|[pi.ino](Arduino/pi.ino)|Arduino, floats|ATtiny1614|December, 2020|
|0.00003|prior|Sun 3/60|C|prior|
|0.00003|prior|Intel 286|C|prior|
|0.000001|prior|Intel 8088|C|prior|

|estimated GFlops|estimated GFlops/W|estimated GFlops/$|code|description|system|date|
|---|---|---|---|---|---|---|
|0.093|0.38|0.012|[pi.ino](Arduino/pi.ino)|Arduino, floats|IMXRT1062<br>Teensy 4.1<br>600 MHz|August, 2021|
|0.0168|0.233|0.004|[pi.c](https://gitlab.cba.mit.edu/zfredin/samd51_benchmarks)|C<br>floats, -O3, gcc-arm-none-eabi, 160 MHz|SAMD51J20A<br>ARM Cortex M4F|October, 2019|
|0.0128|0.171|0.002|[pi.c](https://gitlab.cba.mit.edu/zfredin/stm32f412_core/tree/master/nucleo-f412zg/pi)|C<br>floats, -O3, gcc-arm-none-eabi, 84 MHz|STM32F412<br>ARM Cortex M4F|October, 2019|
