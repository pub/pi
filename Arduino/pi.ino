/*
* pi.ino
* Neil Gershenfeld 12/20/20
* pi calculation benchmark
* pi = 3.14159265358979323846
*/

#define NPTS 100000

float a,b,c,pi,dt,mflops;
unsigned long i,tstart,tend;

void setup() {
   Serial.begin(115200);
   }

void loop() {
   tstart = millis();
   a = 0.5;
   b = 0.75;
   c = 0.25;
   pi = 0;
   for (i = 1; i <= NPTS; ++i)
      pi += a/((i-b)*(i-c));
   tend = millis();
   dt = (tend-tstart)/1000.0;
   mflops = NPTS*5.0/(dt*1e6);
   Serial.print("NPTS = ");
   Serial.print(NPTS);
   Serial.print(" pi = ");
   Serial.println(pi);
   Serial.print("time = ");
   Serial.print(dt);
   Serial.print(" estimated MFlops = ");
   Serial.println(mflops);
   }
